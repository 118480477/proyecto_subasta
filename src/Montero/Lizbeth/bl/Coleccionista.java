
package Montero.Lizbeth.bl;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Atributos de la clase Coleccionista y los atributos relacionbados de otras clases.
 *
 * @author Lizbeth Montero
 * @version 1.0.0
 * @since 1.0.0
 */
public class Coleccionista {
    private String nombre, avatar, estado, correo, direccion, password;
    private double puntuacion;
    private int edad, identificacion;
    private LocalDate fechanacimiento;
    private Administrador administrador;
    private ArrayList<Interes> serieIntereses;
    private ArrayList<Objeto> serieObjetosDuenno;
    private Moderador moderador;


    /**
     * contructor por defecto es utilizado para la invocaciÃ³n de las subclases.
     */
    public Coleccionista() {
    }

    /**
     * Constructoe con todos los parametros propios de la clase Administrador
     *
     * @param nombre          tipo String, nombre del coleccionista.
     * @param avatar          tipo String, identidad virtual del coleccionista.
     * @param estado          tipo String, activo o inactivo del coleccionista.
     * @param correo          tipo String,correo del coleccionista.
     * @param password        tipo String, contraseÃ±a del coleccionista.
     * @param identificacion  tipo int ,identificacion personal del coleccionista.
     * @param edad            tipo int, edad del coleccionista.
     * @param fechanacimiento tipo LocalDate, fecha especifica en la que nacio el coleccionista.
     * @param direccion       tipo String, dereccion del sitio en donde desea recibir la orden de compra el coleccionista.
     * @param puntuacion      tipo double, calificacion de posee el coleccionista a lo largo de su carrera.
     */
    public Coleccionista(String nombre, String avatar, String estado, String correo, String direccion, String password, double puntuacion, int edad, int identificacion, LocalDate fechanacimiento) {
        this.nombre = nombre;
        this.avatar = avatar;
        this.estado = estado;
        this.correo = correo;
        this.direccion = direccion;
        this.password = password;
        this.puntuacion = puntuacion;
        this.edad = edad;
        this.identificacion = identificacion;
        this.fechanacimiento = fechanacimiento;
    }

    /**
     * Constructoe con todos los parametros propios de la clase Administrador y los parametros con los que se relaciona en otras clases respectivas.
     *
     * @param nombre             tipo String, nombre del coleccionista.
     * @param avatar             tipo String, identidad virtual del coleccionista.
     * @param estado             tipo String, activo o inactivo del coleccionista.
     * @param correo             tipo String,correo del coleccionista.
     * @param password           tipo String, contraseÃ±a del coleccionista.
     * @param identificacion     tipo int ,identificacion personal del coleccionista.
     * @param edad               tipo int, edad del coleccionista.
     * @param fechanacimiento    tipo LocalDate, fecha especifica en la que nacio el coleccionista.
     * @param direccion          tipo String, dereccion del sitio en donde desea recibir la orden de compra el coleccionista.
     * @param puntuacion         tipo double, calificacion de posee el coleccionista a lo largo de su carrera.
     * @param administrador      tipo adiministrador, persona que tiene la potestad de eliminar o agregar al coleccionista.
     * @param serieIntereses     tipo interes, lista de intereses que posee el coleccionista.
     * @param serieObjetosDuenno tipo Objeto, serie de objetos de los que es dueÃ±o el coleccionista.
     * @param moderador          tipo Moderador, persona que fiscaliza al coleccionista en la subasta.
     */

    public Coleccionista(String nombre, String avatar, String estado, String correo, String direccion, String password, double puntuacion, int edad, int identificacion, LocalDate fechanacimiento, Administrador administrador, ArrayList<Interes> serieIntereses, ArrayList<Objeto> serieObjetosDuenno, Moderador moderador) {
        this.nombre = nombre;
        this.avatar = avatar;
        this.estado = estado;
        this.correo = correo;
        this.direccion = direccion;
        this.password = password;
        this.puntuacion = puntuacion;
        this.edad = edad;
        this.identificacion = identificacion;
        this.fechanacimiento = fechanacimiento;
        this.administrador = administrador;
        this.serieIntereses = serieIntereses;
        this.serieObjetosDuenno = serieObjetosDuenno;
        this.moderador = moderador;
    }

    /**
     * Metodo Getter de la variable que almacena el nombre, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el nombre del coleccionista, lo hace publico.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param nombre: dato que almacena el nombre del coleccionista de manera privada en su calse respectiva.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo Getter de la variable que almacena el avatar, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el avatar del coleccionista, lo hace publico.
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param avatar: dato que almacena el avatar del coleccionista de manera privada en su calse respectiva.
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * Metodo Getter de la variable que almacena el estado, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el estado del coleccionista, lo hace publico.
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param estado: dato que almacena el estado del coleccionista de manera privada en su calse respectiva.
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Metodo Getter de la variable que almacena el correo, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el correo del coleccionista, lo hace publico.
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param correo: dato que almacena el correo del coleccionista de manera privada en su calse respectiva.
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     * Metodo Getter de la variable que almacena la direccion, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la direccion del coleccionista, lo hace publico.
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param direccion: dato que almacena la direccion del coleccionista de manera privada en su calse respectiva.
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Metodo Getter de la variable que almacena la contrasennia, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la contrasennia del coleccionista, lo hace publico.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param password: dato que almacena la contrasennia del coleccionista de manera privada en su calse respectiva.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Metodo Getter de la variable que almacena la puntuacion, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la puntuacion del coleccionista, lo hace publico.
     */
    public double getPuntuacion() {
        return puntuacion;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param puntuacion: dato que almacena la puntuacion del coleccionista de manera privada en su calse respectiva.
     */
    public void setPuntuacion(double puntuacion) {
        this.puntuacion = puntuacion;
    }

    /**
     * Metodo Getter de la variable que almacena la edad, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la edad del coleccionista, lo hace publico.
     */
    public int getEdad() {
        return edad;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param edad: dato que almacena la edad del coleccionista de manera privada en su calse respectiva.
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * Metodo Getter de la variable que almacena el identificacion, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la identificacion del coleccionista, lo hace publico.
     */
    public int getIdentificacion() {
        return identificacion;
    }

    /**
     * MetÃ³do set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param identificacion: dato que almacena la identificacion del coleccionista de manera privada en su calse respectiva.
     */
    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * Metodo Getter de la variable que almacena la fecha de nacimiento, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la fecha de nacimiento del coleccionista, lo hace publico.
     */
    public LocalDate getFechanacimiento() {
        return fechanacimiento;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechanacimiento: dato que almacena la fecha de nacimiento del coleccionista de manera privada en su calse respectiva.
     */
    public void setFechanacimiento(LocalDate fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    /**
     * Metodo Getter de la variable que almacena el administrador, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el administrador del coleccionista, lo hace publico.
     */
    public Administrador getAdministrador() {
        return administrador;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param administrador: dato que almacena el administrador del coleccionista de manera privada en su calse respectiva.
     */
    public void setAdministrador(Administrador administrador) {
        this.administrador = administrador;
    }

    /**
     * Metodo Getter de la variable que almacena la serieIntereses, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la serieIntereses del coleccionista, lo hace publico.
     */
    public ArrayList<Interes> getSerieIntereses() {
        return serieIntereses;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param serieIntereses: dato que almacena la serieIntereses del coleccionista de manera privada en su calse respectiva.
     */
    public void setSerieIntereses(ArrayList<Interes> serieIntereses) {
        this.serieIntereses = serieIntereses;
    }

    /**
     * Metodo Getter de la variable que almacena la serie Objetos de la que es dueÃ±o, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la serie Objetos de la que es dueÃ±o el coleccionista, lo hace publico.
     */
    public ArrayList<Objeto> getSerieObjetosDuenno() {
        return serieObjetosDuenno;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param serieObjetosDuenno: dato que almacena la serie Objetos de la que es dueÃ±o el coleccionista, coleccionista de manera privada en su calse respectiva.
     */
    public void setSerieObjetosDuenno(ArrayList<Objeto> serieObjetosDuenno) {
        this.serieObjetosDuenno = serieObjetosDuenno;
    }

    /**
     * Metodo Getter de la variable que almacena el moderador, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el moderador del coleccionista, lo hace publico.
     */
    public Moderador getModerador() {
        return moderador;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param moderador: dato que almacena el moderador del coleccionista, coleccionista de manera privada en su calse respectiva.
     */
    public void setModerador(Moderador moderador) {
        this.moderador = moderador;
    }

    /**
     * Metodo utilizado para crear los objetos del coleccionista
     *
     * @param nombre          tipo String, nombre del objeto
     * @param descripcion     tipo String, descripcion del objeto
     * @param estado          tipo String, estado del obajeto, nouevo,viejo...
     * @param imagen          tipo String, imagen del objeto
     * @param categoria       tipo String,  categoria del objeto.
     * @param fechaAntiguedad tipo String, fecha de antiguedad del objeto
     * @param fecha           tipo LocalDate, fecha en que el coleccionista lo adquirio.
     */
    public void agregarObjetos(String nombre, String descripcion, String estado, String imagen, String categoria, String fechaAntiguedad, LocalDate fecha) {
        Objeto objeto = new Objeto(nombre, descripcion, estado, imagen, categoria, fechaAntiguedad, fecha);
        serieObjetosDuenno.add(objeto);
    }

    /**
     * metodo que recorre el ArrayList en donde estan los respectivos objetos del coleccionista.
     *
     * @return devuelve la informacion del el arrayList.
     */
    public String objetosToString() {
        String infoObjetos = "";
        for (Objeto objeto : serieObjetosDuenno) {
            infoObjetos = objeto.toString();
        }
        return infoObjetos;
    }

    /**
     * metodo en donde el coleccionista agrega los intereses que posee al ArraList.
     *
     * @param codigoInteres tipo inte codigo interes, su identificativo.
     * @param nombre        tipo String, nombre del interes.
     * @param descripcion   tipo String descipcion del interes.
     */
    public void agegarIntereses(int codigoInteres, String nombre, String descripcion) {
        Interes interes = new Interes(codigoInteres, nombre, descripcion);
        serieIntereses.add(interes);
    }

    /**
     * metodo que recorre el ArrayList con la serie de intereses del colecccionista.
     *
     * @return devuelve los datos del ArrayList de intereses.
     */
    public String interesesToString() {
        String datosIntereses = "";
        for (Interes interes : serieIntereses) {
            datosIntereses = interes.toString();
        }
        return datosIntereses;
    }

    /**
     * metodo en donde se agrega el moderador del coleccionista.
     *
     * @param nombreModerador tipo String,  nombre dle moderador.
     * @param avatar          tipo String, identificacion virtual del moderadoer, su emoji.
     * @param estado          tipo String, estado del moderador activo, inactivo.
     * @param correo          tipo String,  correo del moderador.
     * @param direccion       tipo String, dirreccion del moderador.
     * @param password        tipo String, contraseÃ±a del moderador.
     * @param identificacion  tipo int, identificacion personal del moderador, cedula.
     * @param edad            tipo int, edad del moderador.
     * @param calificacion    tipo double, puntaje del moderador durante su carrera.
     * @param fechaNacimiento tipo LocalDate, fecha especifica de nacimiento del moderador.
     */
    public void agregarModerador(String nombreModerador, String avatar, String estado, String correo, String direccion, String password, int identificacion, int edad, double calificacion, LocalDate fechaNacimiento) {
        Moderador moderador = new Moderador(nombreModerador, avatar, estado, correo, direccion, password, identificacion, edad, calificacion, fechaNacimiento);
    }

    /**
     * metodo en donde se agrega el administrador del coleccionista.
     *
     * @param nombre          tipo String, nombre del administrador.
     * @param avatar          tipo String, identidad virtual del administrador.
     * @param estado          tipo String, activo o inactivo del administrador.
     * @param correo          tipo String,correo del administrador.
     * @param password        tipo String, contraseÃ±a del administrador.
     * @param identificacion  tipo int ,identificacion personal del administrador.
     * @param edad            tipo int, edad del administrador.
     * @param fechanacimiento tipo LocalDate, fecha especifica en la que nacio el administrador.
     */
    public void agregarAdministrador(String nombre, String avatar, String estado, String correo, String password, int identificacion, int edad, LocalDate fechanacimiento) {
        Administrador administrador = new Administrador(nombre, avatar, estado, correo, password, identificacion, edad, fechanacimiento);
    }

    /**
     * Metodo toString utilizado para imprimir la in informaciÃ³n almacenada en un String asignado.
     *
     * @return retorna los valores respectivos del coleccionista, en este caso el respectivo valor de cada atributo del coleccionista y sus relaciones  en un solo String.
     */
    @Override
    public String toString() {
        return "Coleccionista{" +
                "nombre='" + nombre + '\'' +
                ", avatar='" + avatar + '\'' +
                ", estado='" + estado + '\'' +
                ", correo='" + correo + '\'' +
                ", direccion='" + direccion + '\'' +
                ", password='" + password + '\'' +
                ", puntuacion=" + puntuacion +
                ", edad=" + edad +
                ", identificacion=" + identificacion +
                ", fechanacimiento=" + fechanacimiento +
                ", administrador=" + administrador +
                ", serieIntereses=" + serieIntereses +
                ", serieObjetosDuenno=" + serieObjetosDuenno +
                ", moderador=" + moderador +
                '}';
    }
}
