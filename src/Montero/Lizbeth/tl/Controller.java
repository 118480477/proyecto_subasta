package Montero.Lizbeth.tl;

import Montero.Lizbeth.bl.*;
import Montero.Lizbeth.dl.CapaLogica;

import java.time.LocalDate;

public class Controller {
    private CapaLogica logica;

    public Controller() {
        logica = new CapaLogica();
    }

    public void agregarOrdenCompra(int codigo, double precioTotal, String detalleObjeto, LocalDate fechaDeOrden, Objeto objetoComprado, Ganador nombreGanador) {
        OrdenDeCompra ordenDeCompra = new OrdenDeCompra(codigo, precioTotal, detalleObjeto, fechaDeOrden, objetoComprado, nombreGanador);
        logica.agreagarOrdenCompra(nombreGanador);
    }

    public void agregarObjeto(String nombre, String descripcion, String estado, String imagen, String categoria, String fechaAntiguedad, LocalDate fechaCompra) {
        Objeto objeto = new Objeto(nombre, descripcion, estado, imagen, categoria, fechaAntiguedad, fechaCompra);
        logica.agreagarObjeto(objeto);
    }

    public void imprimirObjeto() {
        logica.objetoToString();
    }
}
