package Montero.Lizbeth.bl;

public class Interes {
    /**
     * Atributos de la clase Interes.
     *  @author Lizbeth Montero
     * @version 1.0.0
     * @since 1.0.0
     */
    private int codigoInteres;
    private String nombre,descripcion;

    /**
     * contructor por defecto es utilizado para la invocación de las subclases.
     */
    public Interes(){}

    /**
     * contructor con los parametros propios de la clase interes.
     * @param codigoInteres tipo int, codigo de interes.
     * @param nombre tipo String nombre del interes.
     * @param descripcion tipo String descripcion del interes.
     */

    public Interes(int codigoInteres, String nombre, String descripcion) {
        this.codigoInteres = codigoInteres;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }
    /**
     *Metodo Getter de la variable que almacena el codigoInteres, es utilizado para obtener informacion del atributo privado de la clase.
     * @return devuelve el codigoInteres del interes, lo hace publico.
     */
    public int getCodigoInteres() {
        return codigoInteres;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     * @param codigoInteres: dato que almacena el codigoInteres  de manera privada en su calse respectiva.
     */
    public void setCodigoInteres(int codigoInteres) {
        this.codigoInteres = codigoInteres;
    }
    /**
     *Metodo Getter de la variable que almacena el nombre, es utilizado para obtener informacion del atributo privado de la clase.
     * @return devuelve el nombre del interes, lo hace publico.
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     * @param nombre: dato que almacena el nombre del interes  de manera privada en su calse respectiva.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     *Metodo Getter de la variable que almacena la descripcion, es utilizado para obtener informacion del atributo privado de la clase.
     * @return devuelve la descripcion del interes, lo hace publico.
     */
    public String getDescripcion() {
        return descripcion;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     * @param descripcion: dato que almacena la descripcion del interes  de manera privada en su calse respectiva.
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     * @return retorna los valores respectivos de los intereses, en este caso el respectivo valor de cada atributo de los intereses   en un solo String.
     */
    @Override
    public String toString() {
        return "Interes{" +
                "codigoInteres=" + codigoInteres +
                ", nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}
