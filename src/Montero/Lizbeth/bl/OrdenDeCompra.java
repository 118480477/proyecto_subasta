package Montero.Lizbeth.bl;

import java.time.LocalDate;
/**
 * Atributos de la clase OrdenDeCompra y los atributos de sus respectivas relaciones.
 * @version 1.0.0
 * @author Lizbeth Montero
 * @since 1.0.0
 */
public class OrdenDeCompra {
    private int codigo;
    private double precioTotal;
    private String detalleObjeto;
    private LocalDate fechaDeOrden;
    private Objeto objetoComprado;
    private Ganador nombreGanador;
    /**
     * contructor por defecto es utilizado para la invocación de las subclases.
     */
    public OrdenDeCompra(){}

    /**
     * contructor con los atributos propios de la clase OrdenCompra.
     * @param codigo tipo int, codigo de la orden.
     * @param precioTotal tipo double, precio total de la ordel.
     * @param detalleObjeto ripo String, detalle especifico del objeto.
     */
    public OrdenDeCompra(int codigo, double precioTotal, String detalleObjeto) {
        this.codigo = codigo;
        this.precioTotal = precioTotal;
        this.detalleObjeto = detalleObjeto;
    }
    /**
     * contructor con los atributos propios de la clase OrdenCompra y sus respectivas relaciones.
     * @param codigo tipo int, codigo de la orden.
     * @param precioTotal tipo double, precio total de la ordel.
     * @param detalleObjeto tipo String, detalle especifico del objeto.
     * @param nombreGanador  tipo Gnador, es el nombre del coleccionista a quien se le genera la orden de compra.
     * @param objetoComprado tipo Objeto, es el objeto en especifico que se compro.
     * @param fechaDeOrden tipo LocalDate, fecha de cuando se realizo la orden.
     */
    public OrdenDeCompra(int codigo, double precioTotal, String detalleObjeto, LocalDate fechaDeOrden, Objeto objetoComprado, Ganador nombreGanador) {
        this.codigo = codigo;
        this.precioTotal = precioTotal;
        this.detalleObjeto = detalleObjeto;
        this.fechaDeOrden = fechaDeOrden;
        this.objetoComprado = objetoComprado;
        this.nombreGanador = nombreGanador;
    }
    /**
     *Metodo Getter de la variable que almacena el codigo , es utilizado para obtener informacion del atributo privado de la clase.
     * @return devuelve el codigo , de la orden de compra, lo hace publico.
     */
    public int getCodigo() {
        return codigo;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     * @param codigo: dato que almacena  el codigo,  de la orden de compra de manera privada en su calse respectiva.
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    /**
     *Metodo Getter de la variable que almacena el precio Total , es utilizado para obtener informacion del atributo privado de la clase.
     * @return devuelve el precio Total , de la orden de compra, lo hace publico.
     */
    public double getPrecioTotal() {
        return precioTotal;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     * @param precioTotal: dato que almacena  el precioTotal,  de la orden de compra de manera privada en su calse respectiva.
     */
    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }
    /**
     *Metodo Getter de la variable que almacena el detalle del Objeto , es utilizado para obtener informacion del atributo privado de la clase.
     * @return devuelve el detalle del Objeto , de la orden de compra, lo hace publico.
     */
    public String getDetalleObjeto() {
        return detalleObjeto;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     * @param detalleObjeto: dato que almacena  el detalle del Objeto,  de la orden de compra de manera privada en su calse respectiva.
     */
    public void setDetalleObjeto(String detalleObjeto) {
        this.detalleObjeto = detalleObjeto;
    }
    /**
     *Metodo Getter de la variable que almacena  la fecha De la Orden , es utilizado para obtener informacion del atributo privado de la clase.
     * @return devuelve la fecha De la Orden , de la orden de compra, lo hace publico.
     */
    public LocalDate getFechaDeOrden() {
        return fechaDeOrden;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     * @param fechaDeOrden: dato que almacena  la fecha De la Orden,  de la orden de compra de manera privada en su calse respectiva.
     */
    public void setFechaDeOrden(LocalDate fechaDeOrden) {
        this.fechaDeOrden = fechaDeOrden;
    }
    /**
     *Metodo Getter de la variable que almacena el objetoComprado , es utilizado para obtener informacion del atributo privado de la clase.
     * @return devuelve el objetoComprado  , de la orden de compra, lo hace publico.
     */
    public Objeto getObjetoComprado() {
        return objetoComprado;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     * @param objetoComprado: dato que almacena el objetoComprado ,  de la orden de compra de manera privada en su calse respectiva.
     */
    public void setObjetoComprado(Objeto objetoComprado) {
        this.objetoComprado = objetoComprado;
    }
    /**
     *Metodo Getter de la variable que almacena el nombre del Ganador , es utilizado para obtener informacion del atributo privado de la clase.
     * @return devuelve el nombre del Ganador  , de la orden de compra, lo hace publico.
     */
    public Ganador getNombreGanador() {
        return nombreGanador;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     * @param nombreGanador: dato que almacena el nombre del Ganador ,  de la orden de compra de manera privada en su calse respectiva.
     */
    public void setNombreGanador(Ganador nombreGanador) {
        this.nombreGanador = nombreGanador;
    }
    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     * @return retorna los valores respectivos de la OrdenDeCompra,con el respectivo valor de cada atributo en un solo String.
     */
    @Override
    public String toString() {
        return "OrdenDeCompra{" +
                "codigo=" + codigo +
                ", precioTotal=" + precioTotal +
                ", detalleObjeto='" + detalleObjeto + '\'' +
                ", fechaDeOrden=" + fechaDeOrden +
                ", objetoComprado=" + objetoComprado +
                ", nombreGanador=" + nombreGanador +
                '}';
    }


}
