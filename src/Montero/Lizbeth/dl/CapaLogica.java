package Montero.Lizbeth.dl;

import Montero.Lizbeth.bl.*;

import java.util.ArrayList;

public class CapaLogica {
    private ArrayList<Objeto> objetoDelVendedor;
    private Ganador ordenCompra;
    public CapaLogica(){}

    public ArrayList<Objeto> getObjetoDelVendedor() {
        return objetoDelVendedor;
    }

    public Ganador getOrdenCompra() {
        return ordenCompra;
    }

    public void setOrdenCompra(Ganador ordenCompra) {
        this.ordenCompra = ordenCompra;
    }

    public void setObjetoDelVendedor(ArrayList<Objeto> objetoDelVendedor) {
        this.objetoDelVendedor = objetoDelVendedor;
    }
    public void agreagarOrdenCompra( Ganador ordenDeCompra){
        ordenDeCompra.add(ordenDeCompra);
    }

    public void agreagarObjeto( Objeto objeto){
        objetoDelVendedor.add(objeto);
    }

    public String objetoToString() {
        String datoObjeto = "";
        for (Objeto objeto : objetoDelVendedor) datoObjeto += objeto.toString();
        return datoObjeto;
    }
}
