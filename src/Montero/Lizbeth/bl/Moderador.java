package Montero.Lizbeth.bl;

import java.time.LocalDate;

/**
 * Atributos de la clase Moderador.
 *
 * @author Lizbeth Montero
 * @version 1.0.0
 * @since 1.0.0
 */
public class Moderador {
    private String nombreModerador, avatar, estado, correo, direccion, password;
    private int identificacion, edad;
    private double calificacion;
    private LocalDate fechaNacimiento;

    /**
     * contructor por defecto es utilizado para la invocación de las subclases.
     */
    public Moderador() {
    }

    /**
     * Contructor que recibe todos los parametros.
     * metodo en donde se agrega el moderador del coleccionista.
     *
     * @param nombreModerador tipo String,  nombre dle moderador.
     * @param avatar          tipo String, identificacion virtual del moderadoer, su emoji.
     * @param estado          tipo String, estado del moderador activo, inactivo.
     * @param correo          tipo String,  correo del moderador.
     * @param direccion       tipo String, dirreccion del moderador.
     * @param password        tipo String, contraseña del moderador.
     * @param identificacion  tipo int, identificacion personal del moderador, cedula.
     * @param edad            tipo int, edad del moderador.
     * @param calificacion    tipo double, puntaje del moderador durante su carrera.
     * @param fechaNacimiento tipo LocalDate, fecha especifica de nacimiento del moderador.
     */
    public Moderador(String nombreModerador, String avatar, String estado, String correo, String direccion, String password, int identificacion, int edad, double calificacion, LocalDate fechaNacimiento) {
        this.nombreModerador = nombreModerador;
        this.avatar = avatar;
        this.estado = estado;
        this.correo = correo;
        this.direccion = direccion;
        this.password = password;
        this.identificacion = identificacion;
        this.edad = edad;
        this.calificacion = calificacion;
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     * Metodo Getter de la variable que almacena el nombreModerador, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el nombre del moderador, lo hace publico.
     */
    public String getNombreModerador() {
        return nombreModerador;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param nombreModerador: dato que almacena  el nombre del moderador  de manera privada en su calse respectiva.
     */
    public void setNombreModerador(String nombreModerador) {
        this.nombreModerador = nombreModerador;
    }

    /**
     * Metodo Getter de la variable que almacena el avatar, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el avatar del moderador, lo hace publico.
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param avatar: dato que almacena  el avatar del moderador  de manera privada en su calse respectiva.
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * Metodo Getter de la variable que almacena el estado, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el estado del moderador, lo hace publico.
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param estado: dato que almacena  el estado del moderador  de manera privada en su calse respectiva.
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Metodo Getter de la variable que almacena el correo, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el correo del moderador, lo hace publico.
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param correo: dato que almacena  el correo del moderador  de manera privada en su calse respectiva.
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     * Metodo Getter de la variable que almacena la direccion, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la direccion del moderador, lo hace publico.
     */

    public String getDireccion() {
        return direccion;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param direccion: dato que almacena la direccion del moderador  de manera privada en su calse respectiva.
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Metodo Getter de la variable que almacena la contraseña, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  la contraseña del moderador, lo hace publico.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param password: dato que almacena la  contraseñadel moderador  de manera privada en su calse respectiva.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Metodo Getter de la variable que almacena la identificacion, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  la identificacion del moderador, lo hace publico.
     */
    public int getIdentificacion() {
        return identificacion;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param identificacion: dato que almacena la  identificacion del moderador  de manera privada en su calse respectiva.
     */
    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * Metodo Getter de la variable que almacena la edad, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  la edad del moderador, lo hace publico.
     */
    public int getEdad() {
        return edad;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param edad: dato que almacena la  edad del moderador  de manera privada en su calse respectiva.
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * Metodo Getter de la variable que almacena la calificacion, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  la calificacion del moderador, lo hace publico.
     */
    public double getCalificacion() {
        return calificacion;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param calificacion: dato que almacena la  calificacion del moderador  de manera privada en su calse respectiva.
     */
    public void setCalificacion(double calificacion) {
        this.calificacion = calificacion;
    }

    /**
     * Metodo Getter de la variable que almacena la fecha de Nacimiento, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  la fecha de Nacimiento del moderador, lo hace publico.
     */
    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechaNacimiento: dato que almacena la  fecha de Nacimiento del moderador  de manera privada en su calse respectiva.
     */
    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos del moderador, en este caso el respectivo valor de cada atributo en un solo String.
     */
    @Override
    public String toString() {
        return "Moderador{" +
                "nombreModerador='" + nombreModerador + '\'' +
                ", avatar='" + avatar + '\'' +
                ", estado='" + estado + '\'' +
                ", correo='" + correo + '\'' +
                ", direccion='" + direccion + '\'' +
                ", password='" + password + '\'' +
                ", identificacion=" + identificacion +
                ", edad=" + edad +
                ", calificacion=" + calificacion +
                ", fechaNacimiento=" + fechaNacimiento +
                '}';
    }


}
