package Montero.Lizbeth.bl;

import java.time.LocalDate;

/**
 * Atributos de la clase ganador y los atributos relacionbados de otras clases.
 *
 * @author Lizbeth Montero
 * @version 1.0.0
 * @since 1.0.0
 */
public class Ganador {
    private int codigoGanador;
    private boolean valido;
    private Oferta ofertaGanada;
    private OrdenDeCompra ordenDeCompra;
    private Coleccionista coleccionista;

    /**
     * contructor por defecto es utilizado para la invocación de las subclases.
     */
    public Ganador() {
    }

    /**
     * contructor con los atributos propios del ganador.
     *
     * @param codigoGanador tipo int, codigo de canador, su identificacion.
     * @param valido        tipo boleean, es para verificar si verdadermente es ganador o no.
     */
    public Ganador(int codigoGanador, boolean valido) {
        this.codigoGanador = codigoGanador;
        this.valido = valido;
    }

    /**
     * constructor con todos los atributos, tanto los propios como sus relaciones.
     *
     * @param codigoGanador tipo int, codigo de canador, su identificacion.
     * @param valido        tipo boleean, es para verificar si verdadermente es ganador o no.
     * @param ofertaGanada  tipo Oferta, la oferta que aprovecho, por la cual dio mas dinero.
     * @param ordenDeCompra tipo OrdenCompra la orden de compra generada despues de haber ganado.
     * @param coleccionista tipo Coleccionista, nombre del coleccionista ganador.
     */
    public Ganador(int codigoGanador, boolean valido, Oferta ofertaGanada, OrdenDeCompra ordenDeCompra, Coleccionista coleccionista) {
        this.codigoGanador = codigoGanador;
        this.valido = valido;
        this.ofertaGanada = ofertaGanada;
        this.ordenDeCompra = ordenDeCompra;
        this.coleccionista = coleccionista;
    }

    /**
     * Metodo Getter de la variable que almacena el codigoGanador, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el codigoGanador del ganador, lo hace publico.
     */
    public int getCodigoGanador() {
        return codigoGanador;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param codigoGanador: dato que almacena el codigoGanador del ganador de manera privada en su calse respectiva.
     */
    public void setCodigoGanador(int codigoGanador) {
        this.codigoGanador = codigoGanador;
    }

    /**
     * Metodo Getter de la variable que almacena el valido, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el valido del ganador, lo hace publico.
     */
    public boolean isValido() {
        return valido;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param valido: dato que almacena el valor del ganador de manera privada en su calse respectiva.
     */
    public void setValido(boolean valido) {
        this.valido = valido;
    }

    /**
     * Metodo Getter de la variable que almacena la ofertaGanada, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la ofertaGanada del ganador, lo hace publico.
     */
    public Oferta getOfertaGanada() {
        return ofertaGanada;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param ofertaGanada: dato que almacena la ofertaGanada del ganador de manera privada en su calse respectiva.
     */
    public void setOfertaGanada(Oferta ofertaGanada) {
        this.ofertaGanada = ofertaGanada;
    }

    /**
     * Metodo Getter de la variable que almacena la ordenDeCompra, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la ordenDeCompra del ganador, lo hace publico.
     */
    public OrdenDeCompra getOrdenDeCompra() {
        return ordenDeCompra;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param ordenDeCompra: dato que almacena la ordenDeCompra del ganador de manera privada en su calse respectiva.
     */
    public void setOrdenDeCompra(OrdenDeCompra ordenDeCompra) {
        this.ordenDeCompra = ordenDeCompra;
    }

    /**
     * Metodo Getter de la variable que almacena el coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el coleccionista ganador, lo hace publico.
     */
    public Coleccionista getColeccionista() {
        return coleccionista;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param coleccionista: dato que almacena el coleccionista ganador de manera privada en su calse respectiva.
     */
    public void setColeccionista(Coleccionista coleccionista) {
        this.coleccionista = coleccionista;
    }

    /**
     * metodo utilizado para agregar el coleccionista ganador de la subasta.
     *
     * @param nombre          tipo String, nombre del coleccionista.
     * @param avatar          tipo String, identidad virtual del coleccionista.
     * @param estado          tipo String, activo o inactivo del coleccionista.
     * @param correo          tipo String,correo del coleccionista.
     * @param password        tipo String, contraseña del coleccionista.
     * @param identificacion  tipo int ,identificacion personal del coleccionista.
     * @param edad            tipo int, edad del coleccionista.
     * @param fechanacimiento tipo LocalDate, fecha especifica en la que nacio el coleccionista.
     * @param direccion       tipo String, dereccion del sitio en donde desea recibir la orden de compra el coleccionista.
     * @param puntuacion      tipo double, calificacion de posee el coleccionista a lo largo de su carrera.
     */
    public void agregarColeccionista(String nombre, String avatar, String estado, String correo, String direccion, String password, double puntuacion, int edad, int identificacion, LocalDate fechanacimiento) {
        Coleccionista coleccionista;
        coleccionista = new Coleccionista(nombre, avatar, estado, correo, direccion, password, puntuacion, edad, identificacion, fechanacimiento);
    }

    /**
     * metodo utilizado para agregarle la orden de compra al ganador.
     *
     * @param codigo         tipo int, codigo de la orden.
     * @param precioTotal    tipo double, precio final de la compra.
     * @param detalleObjeto  tipo String datalle especifico del objeto.
     * @param fechaDeOrden   tipo LocalDate de la orden, fecha especifica en la que se realiza la orden de compra.
     * @param objetoComprado tipo Objeto, objeto comprado por el coleccionista ganador.
     * @param nombreGanador  tipo Ganador, nombre del coleccionista ganador.
     */
    public void agregarOrdenCompra(int codigo, double precioTotal, String detalleObjeto, LocalDate fechaDeOrden, Objeto objetoComprado, Ganador nombreGanador) {
        OrdenDeCompra ordenDeCompra = new OrdenDeCompra(codigo, precioTotal, detalleObjeto, fechaDeOrden, objetoComprado, nombreGanador);
    }

    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos del coleccionista, en este caso el respectivo valor de cada atributo del ganador y sus relaciones  en un solo String.
     */
    @Override
    public String toString() {
        return "Ganador{" +
                "codigoGanador=" + codigoGanador +
                ", valido=" + valido +
                ", ofertaGanada=" + ofertaGanada +
                ", ordenDeCompra=" + ordenDeCompra +
                ", coleccionista=" + coleccionista +
                '}';
    }

    /**
     * metodo que se utiliza para agregarle una orden de compra al ganador
     *
     * @param ordenDeCompra tipo OrdenCompra, la factura de los objetos comprados.
     */
    public void add(Ganador ordenDeCompra) {
        ordenDeCompra.add(ordenDeCompra);
    }
}

