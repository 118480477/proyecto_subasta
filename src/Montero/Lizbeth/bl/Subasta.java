package Montero.Lizbeth.bl;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Atributos de la clase Subasta y los atributos de sus respectivas relaciones.
 *
 * @author Lizbeth Montero
 * @version 1.0.0
 * @since 1.0.0
 */

public class Subasta {
    private int codigo;
    private LocalDate fechaCreacion, fechaInicio, FechaVencimiento;
    private int horaInicio;
    private Objeto objetosAVender;
    private double puntuacionPromedio, precioMinimoInicial;
    private String estadoSubasta, nombreVendedor, nombreDelColeccionista;
    private Ganador ganador;
    private Moderador moderadorEncargado;
    private ArrayList<Oferta> ofertas;

    /**
     * contructor por defecto es utilizado para la invocación de las subclases.
     */

    public Subasta() {
    }

    /**
     * Constructor con los atributos propios de la clase subasta.
     *
     * @param codigo                 tipo int, codigo de la subasta.
     * @param fechaCreacion          tipo LocalDate, fecha en que se crea la subasta.
     * @param fechaInicio            tipo LocalDate, fecha en la que se inicia la subasta.
     * @param fechaVencimiento       tipo LocalDate, fecha en la que se termina la subasta.
     * @param horaInicio             tipo int, hora en la que inicia la subasta.
     * @param objetosAVender         tipo Obejto, objeto que se va a subastar.
     * @param puntuacionPromedio     tipo double, la puntuacion de le dan a la subasta.
     * @param precioMinimoInicial    tipo double, precio base con el que se inicia la subasta.
     * @param estadoSubasta          tipo String, estado en el que se encuentra la subasta, abierta, cerrada.
     * @param nombreVendedor         tipo String, nombre del vendedor de los articulos que se encuentran el la subasta.
     * @param nombreDelColeccionista tipo String nombre del coleccionista que esta en la subasta.
     */
    public Subasta(int codigo, LocalDate fechaCreacion, LocalDate fechaInicio, LocalDate fechaVencimiento, int horaInicio, Objeto objetosAVender, double puntuacionPromedio, double precioMinimoInicial, String estadoSubasta, String nombreVendedor, String nombreDelColeccionista) {
        this.codigo = codigo;
        this.fechaCreacion = fechaCreacion;
        this.fechaInicio = fechaInicio;
        FechaVencimiento = fechaVencimiento;
        this.horaInicio = horaInicio;
        this.objetosAVender = objetosAVender;
        this.puntuacionPromedio = puntuacionPromedio;
        this.precioMinimoInicial = precioMinimoInicial;
        this.estadoSubasta = estadoSubasta;
        this.nombreVendedor = nombreVendedor;
        this.nombreDelColeccionista = nombreDelColeccionista;
    }

    /**
     * Constructor con los atributos propios de la clase subasta y sus respectivas relaciones.
     *
     * @param codigo                 tipo int, codigo de la subasta.
     * @param fechaCreacion          tipo LocalDate, fecha en que se crea la subasta.
     * @param fechaInicio            tipo LocalDate, fecha en la que se inicia la subasta.
     * @param fechaVencimiento       tipo LocalDate, fecha en la que se termina la subasta.
     * @param horaInicio             tipo int, hora en la que inicia la subasta.
     * @param objetosAVender         tipo Obejto, objeto que se va a subastar.
     * @param puntuacionPromedio     tipo double, la puntuacion de le dan a la subasta.
     * @param precioMinimoInicial    tipo double, precio base con el que se inicia la subasta.
     * @param estadoSubasta          tipo String, estado en el que se encuentra la subasta, abierta, cerrada.
     * @param nombreVendedor         tipo String, nombre del vendedor de los articulos que se encuentran el la subasta.
     * @param nombreDelColeccionista tipo String nombre del coleccionista que esta en la subasta.
     * @param ganador                tipo ganador, datos del ganadoer de la subasta.
     * @param moderadorEncargado     tipo moderador, moderador encargado de la subasta.
     * @param ofertas                tipo ArrayList se guardan las ofertas que se le realizan a un item.
     */
    public Subasta(int codigo, LocalDate fechaCreacion, LocalDate fechaInicio, LocalDate fechaVencimiento, int horaInicio, Objeto objetosAVender, double puntuacionPromedio, double precioMinimoInicial, String estadoSubasta, String nombreVendedor, String nombreDelColeccionista, Ganador ganador, Moderador moderadorEncargado, ArrayList<Oferta> ofertas) {
        this.codigo = codigo;
        this.fechaCreacion = fechaCreacion;
        this.fechaInicio = fechaInicio;
        FechaVencimiento = fechaVencimiento;
        this.horaInicio = horaInicio;
        this.objetosAVender = objetosAVender;
        this.puntuacionPromedio = puntuacionPromedio;
        this.precioMinimoInicial = precioMinimoInicial;
        this.estadoSubasta = estadoSubasta;
        this.nombreVendedor = nombreVendedor;
        this.nombreDelColeccionista = nombreDelColeccionista;
        this.ganador = ganador;
        this.moderadorEncargado = moderadorEncargado;
        this.ofertas = ofertas;
    }

    /**
     * Metodo Getter de la variable que almacena el codigo de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el codigo de la subasta, lo hace publico.
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param codigo: dato que almacena el codigo de la subasta manera privada en su calse respectiva.
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * Metodo Getter de la variable que almacena la fecha de Creacion de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la fecha de Creacion de la subasta, lo hace publico.
     */
    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechaCreacion: dato que almacena la fecha de Creacion de la subasta manera privada en su calse respectiva.
     */
    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    /**
     * Metodo Getter de la variable que almacena la fecha de Inicio de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la fecha de Inicio de la subasta, lo hace publico.
     */
    public LocalDate getFechaInicio() {
        return fechaInicio;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechaInicio: dato que almacena la fecha de Inicio de la subasta manera privada en su calse respectiva.
     */
    public void setFechaInicio(LocalDate fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    /**
     * Metodo Getter de la variable que almacena la Fecha de Vencimientode la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la Fecha de Vencimiento de la subasta, lo hace publico.
     */
    public LocalDate getFechaVencimiento() {
        return FechaVencimiento;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechaVencimiento: dato que almacena la e fecha de Vencimiento de la subasta manera privada en su calse respectiva.
     */
    public void setFechaVencimiento(LocalDate fechaVencimiento) {
        FechaVencimiento = fechaVencimiento;
    }

    /**
     * Metodo Getter de la variable que almacena la hora de Inicio la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la horaInicio de la subasta, lo hace publico.
     */
    public int getHoraInicio() {
        return horaInicio;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param horaInicio: dato que almacena la hora de Inicio de la subasta manera privada en su calse respectiva.
     */
    public void setHoraInicio(int horaInicio) {
        this.horaInicio = horaInicio;
    }

    /**
     * Metodo Getter de la variable que almacena  los objetosAVender la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve los objetosAVender de la subasta, lo hace publico.
     */
    public Objeto getObjetosAVender() {
        return objetosAVender;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param objetosAVender: dato que almacena los objetosAVender de la subasta manera privada en su calse respectiva.
     */
    public void setObjetosAVender(Objeto objetosAVender) {
        this.objetosAVender = objetosAVender;
    }

    /**
     * Metodo Getter de la variable que almacena  la puntuacionPromedio de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la puntuacionPromedio de la subasta, lo hace publico.
     */
    public double getPuntuacionPromedio() {
        return puntuacionPromedio;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param puntuacionPromedio: dato que almacena la puntuacionPromedio de la subasta manera privada en su calse respectiva.
     */
    public void setPuntuacionPromedio(double puntuacionPromedio) {
        this.puntuacionPromedio = puntuacionPromedio;
    }

    /**
     * Metodo Getter de la variable que almacena el precioMinimoInicial de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el precioMinimoInicial de la subasta, lo hace publico.
     */
    public double getPrecioMinimoInicial() {
        return precioMinimoInicial;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param precioMinimoInicial: dato que almacena el precioMinimoInicial de la subasta manera privada en su calse respectiva.
     */
    public void setPrecioMinimoInicial(double precioMinimoInicial) {
        this.precioMinimoInicial = precioMinimoInicial;
    }

    /**
     * Metodo Getter de la variable que almacena el estadoSubasta de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el estadoSubasta de la subasta, lo hace publico.
     */
    public String getEstadoSubasta() {
        return estadoSubasta;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param estadoSubasta: dato que almacena el estadoSubasta de la subasta manera privada en su calse respectiva.
     */
    public void setEstadoSubasta(String estadoSubasta) {
        this.estadoSubasta = estadoSubasta;
    }

    /**
     * Metodo Getter de la variable que almacena el nombreVendedor de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el nombreVendedor de la subasta, lo hace publico.
     */
    public String getNombreVendedor() {
        return nombreVendedor;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param nombreVendedor: dato que almacena el nombreVendedor de la subasta manera privada en su calse respectiva.
     */
    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    /**
     * Metodo Getter de la variable que almacena el nombreDelColeccionista de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el nombreDelColeccionista de la subasta, lo hace publico.
     */
    public String getNombreDelColeccionista() {
        return nombreDelColeccionista;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param nombreDelColeccionista: dato que almacena el nombreDelColeccionista de la subasta manera privada en su calse respectiva.
     */
    public void setNombreDelColeccionista(String nombreDelColeccionista) {
        this.nombreDelColeccionista = nombreDelColeccionista;
    }

    /**
     * Metodo Getter de la variable que almacena el ganador de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el ganador de la subasta, lo hace publico.
     */
    public Ganador getGanador() {
        return ganador;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param ganador: dato que almacena el ganador de la subasta manera privada en su calse respectiva.
     */
    public void setGanador(Ganador ganador) {
        this.ganador = ganador;
    }

    /**
     * Metodo Getter de la variable que almacena el moderadorEncargado de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el moderadorEncargado de la subasta, lo hace publico.
     */
    public Moderador getModeradorEncargado() {
        return moderadorEncargado;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param moderadorEncargado: dato que almacena el moderadorEncargado de la subasta manera privada en su calse respectiva.
     */
    public void setModeradorEncargado(Moderador moderadorEncargado) {
        this.moderadorEncargado = moderadorEncargado;
    }

    /**
     * Metodo Getter de la variable que almacena las ofertas de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve las ofertas de la subasta, lo hace publico.
     */
    public ArrayList<Oferta> getOfertas() {
        return ofertas;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param ofertas: dato que almacena las ofertas de la subasta manera privada en su calse respectiva.
     */
    public void setOfertas(ArrayList<Oferta> ofertas) {
        this.ofertas = ofertas;
    }

    /**
     * metodo utilizado para agregar las ofertas a la subasta.
     *
     * @param idOferente         tipo int, codigo de oferta.
     * @param puntuacionOferente tipo double, la puntuacion del coleccionista.
     * @param precioOfertado     tipo double, precio que le ofrecen a un articulo.
     * @param fechaOferta        tipo LocalDate, fecha en que se hace la oferta
     * @param coleccionistas     tipo Coleccionista, coleccionista que hace la oferta.
     * @param objetoSubastado    tipo Objeto, objeto al que se le está haciendo la oferta.
     */
    public void agregarOfertas(int idOferente, double puntuacionOferente, double precioOfertado, Objeto objetoSubastado, LocalDate fechaOferta, Coleccionista coleccionistas) {
        Oferta oferta = new Oferta(idOferente, precioOfertado, fechaOferta, objetoSubastado, coleccionistas, puntuacionOferente);
    }

    /**
     * metodo utilizado para agregar el moderador a la subasta.
     *
     * @param nombreModerador tipo String,  nombre dle moderador.
     * @param avatar          tipo String, identificacion virtual del moderadoer, su emoji.
     * @param estado          tipo String, estado del moderador activo, inactivo.
     * @param correo          tipo String,  correo del moderador.
     * @param direccion       tipo String, dirreccion del moderador.
     * @param password        tipo String, contraseña del moderador.
     * @param identificacion  tipo int, identificacion personal del moderador, cedula.
     * @param edad            tipo int, edad del moderador.
     * @param calificacion    tipo double, puntaje del moderador durante su carrera.
     * @param fechaNacimiento tipo LocalDate, fecha especifica de nacimiento del moderador.
     */
    public void agregarModerador(String nombreModerador, String avatar, String estado, String correo, String direccion, String password, int identificacion, int edad, double calificacion, LocalDate fechaNacimiento) {
        Moderador moderador = new Moderador(nombreModerador, avatar, estado, correo, direccion, password, identificacion, edad, calificacion, fechaNacimiento);
    }

    /**
     * metodo utilizado para agregar el ganador a la subasta.
     *
     * @param codigoGanador tipo int, codigo de canador, su identificacion.
     * @param valido        tipo boleean, es para verificar si verdadermente es ganador o no.
     * @param ofertaGanada  tipo Oferta, la oferta que aprovecho, por la cual dio mas dinero.
     * @param ordenDeCompra tipo OrdenCompra la orden de compra generada despues de haber ganado.
     * @param coleccionista tipo Coleccionista, nombre del coleccionista ganador.
     */
    public void agregarGanador(int codigoGanador, boolean valido, Oferta ofertaGanada, OrdenDeCompra ordenDeCompra, Coleccionista coleccionista) {
        Ganador ganador = new Ganador(codigoGanador, valido, ofertaGanada, ordenDeCompra, coleccionista);
    }

    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos de la subasta ,con el respectivo valor de cada atributo en un solo String.
     */
    @Override
    public String toString() {
        return "Subasta{" +
                "codigo=" + codigo +
                ", fechaCreacion=" + fechaCreacion +
                ", fechaInicio=" + fechaInicio +
                ", FechaVencimiento=" + FechaVencimiento +
                ", horaInicio=" + horaInicio +
                ", objetosAVender=" + objetosAVender +
                ", puntuacionPromedio=" + puntuacionPromedio +
                ", precioMinimoInicial=" + precioMinimoInicial +
                ", estadoSubasta='" + estadoSubasta + '\'' +
                ", nombreVendedor='" + nombreVendedor + '\'' +
                ", nombreDelColeccionista='" + nombreDelColeccionista + '\'' +
                ", ganador=" + ganador +
                ", moderadorEncargado=" + moderadorEncargado +
                ", ofertas=" + ofertas +
                '}';
    }
}
